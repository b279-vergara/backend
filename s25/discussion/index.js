// console.log("Hello Wolrd");

// [SECTION] JSON Object
/*
	- JSON stands for JavaScript Object Notation
	- JSON is also used in other programming languages hence the name JavaScript Object Notation
	- Core JavaScript has a built in JSON object that contains methods for parsing JSON objects and converting strings into JavaScript objects
	- JavaScript objects are not to be confused with JSON
- JSON is used for serializing different data types into bytes
	- Serialization is the process of converting data into a series of bytes for easier transmission/transfer of information
	- A byte is a unit of data that is eight binary digits (1 and 0) that is used to represent a character (letters, numbers or typographic symbols)
- Bytes are information that a computer processes to perform different tasks
	- Uses double quotes for property names
	- Syntax
		{
			"propertyA": "valueA",
			"propertyB": "valueB",
		}
*/

// JSON Objects

/*

{
	"city" : "Quezon City",
	"province" : "Metro Manila"
	"country" : "Philippines"

}

*/

//  [SECTION] JSON Arrays

/*

"cities" : [
	{"city" : "Quezon City", }
]

*/