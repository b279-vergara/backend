db.fruits.aggregate([
  {$match: {onSale: true}},
  {$group: {_id: "$name", fruitsOnSale: {$sum: 1}}},
  {$group: {_id: null, totalFruitsOnSale: {$sum: "$fruitsOnSale"}}},
  {$project: {_id: 0}}
]);

db.fruits.aggregate([
  {$match: {stock: {$gte: 20}}},
  {$group: {_id: "$name", enoughStock: {$sum: 1}}},
  {$group: {_id: null, enoughStock: {$sum: 1}}},
  {$project: {_id: 0}}
]);

db.fruits.aggregate([
  {$match: {onSale: true}},
  {$group: {_id: "$supplier_id", avg_price: {$avg: "$price"}}},
  {$project: {_id: 0, supplier_id: "$_id", avg_price : 1}}
]);

db.fruits.aggregate([
  {$group: {_id: {supplier: "$supplier_id", name: "$name"}, maxPrice: {$max: "$price"}}},
  {$group: {_id: "$_id.supplier", highestPrice: {$max: "$maxPrice"}}},
  {$project: {_id: 0, supplier_id: "$_id", highestPrice: 1}}
]);

db.fruits.aggregate([
  {$group: {_id: {supplier: "$supplier_id", name: "$name"}, minPrice: {$min: "$price"}}},
  {$group: {_id: "$_id.supplier", lowestPrice: {$min: "$minPrice"}}},
  {$project: {_id: 0, supplier_id: "$_id", lowestPrice: 1}}
]);