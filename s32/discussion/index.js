let http = require("http");

http.createServer(function(req, res){
    // CRUD --> RETRIEVE
    // GET Method is somehow equal to retrieve method in CRUD
    if(req.url == "/items" && req.method == "GET"){
        res.writeHead(200, {"Content-Type" : "text/plain" })
        res.end("Data retrieve from the database.");
    }

    // POST Method is used for creating/sending data to our database
    if(req.url == "/items" && req.method == "POST"){
        res.writeHead(200, {"Content-Type" : "text/plain" })
        res.end("Data to be sent to the database.");
    }

}).listen(4000);

console.log("Server running at localhost:4000")