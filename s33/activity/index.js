let todos;
let items = [];
let i = 0;


fetch("https://jsonplaceholder.typicode.com/todos")
.then(res => res.json())
.then(json => {
    todos = json;
    let todosMap = todos.map(function(todo){
        items[i] = todo.title;
        i++;
        return items;
        
    });
    console.log(items)
    
});

fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(res => res.json())
.then(json => {console.log(`The status of "${json.title}" is ${json.completed}`)});


fetch('https://jsonplaceholder.typicode.com/todos', {
    method: "POST",
    headers: {"Content-Type" : "application/json"},
    body: JSON.stringify({
        title: "New Item",
        completed: false,
        userId: 1
    })
})
.then(res => res.json())
.then(json => console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "PUT",
    headers: {"Content-Type" : "application/json"},
    body: JSON.stringify({
        id: 1,
        title: "Updated todo list item",
        body: "Hello Again",
        status: "pending",
        description:"added description",
        dateCompleted: "Pending",
        userId: 1
    })
})
.then(res => res.json())
.then(json => console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "PATCH",
    headers: {"Content-Type" : "application/json"},
    body: JSON.stringify({
        dateCompleted: "05/03/2023",
        status: true
    })
})
.then(res => res.json())
.then(json => console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "DELETE"
});