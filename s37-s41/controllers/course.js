    const Course = require('../models/Course');


module.exports.addCourse = (data) => {
	console.log(data.isAdmin);
	if(data.isAdmin) {
		let newCourse = new Course({
			name: data.course.name,
			description: data.course.description,
			price: data.course.price
		})

		return newCourse.save().then((course, error) => {
			if(error){
				return error;
			}
			return course;
		})
	} 

	// If the user is not admin, then return this message as a promise to avoid errors
	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})
}

module.exports.getAllCourses = () => {
    return Course.find({}).then(result =>{
        return result;
    })

}

module.exports.getAllActive = () => {
    return Course.find({isActive : true}).then(result =>{
        return result;
    })

}

module.exports.getCourse = (reqParams) => {
    return Course.findById(reqParams.courseId).then(result => {
        return result;
    })
}

module.exports.updateCourse = (reqParams, reqBody, isAdmin) => {
    console.log(isAdmin)
    if(isAdmin){
        let updateCourse = {
            name : reqBody.name,
            description : reqBody.description,
            price : reqBody.price
        }
        
        return Course.findByIdAndUpdate(reqParams.courseId, updateCourse).then((course, error) => {
            if(error){
                return false;
            }else{
                return "course updated";
            }
        })
    }else{
        return Promise.resolve("you not admin bishhh")
    }
}

module.exports.archiveCourse = (reqParams, reqBody, isAdmin) => {
    console.log(isAdmin)
    if(isAdmin){
        let updateCourse = {
            isActive : reqBody.isActive
        }
        
        return Course.findByIdAndUpdate(reqParams.courseId, updateCourse).then((course, error) => {
            if(error){
                return false;
            }else{
                return "course archived";
            }
        })
    }else{
        return Promise.resolve("you not admin bishhh")
    }
}