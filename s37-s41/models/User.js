const  mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
    firstName : {
        type : String,
        required : [true, "FIRST NAME is required!"]
    },
    lastName : {
        type : String,
        required : [true, "LAST NAME is required!"]
    },
    email : {
        type : String,
        required : [true, "EMAIL is required!"]
    },
    password : {
        type : String,
        required : [true, "PASSWORD is required!"]
    },
    isAdmin : {
        type : Boolean,
        default : false
    },
    mobileNo : {
        type : String,
        required : [true, "MOBILE NO is required!"]
    },
    enrollments : [
        {
            courseId : {
                type : String,
                required : [true, "COURSE ID is required!"]
            },
            enrolledOn : {
                type : Date,
                default : new Date()
            },
            status: {
                type : String,
                default: "enrolled"
            }
        }
    ]

})

module.exports = mongoose.model("User", userSchema);