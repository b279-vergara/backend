const express = require("express");
const router = express.Router();
const courseController = require("../controllers/course.js");
const auth = require("../auth.js");

// Route for creating a course
router.post("/", auth.verify, (req, res) => {
	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	courseController.addCourse(data).then(resultFromController => res.send(resultFromController));
});


// Allows us to export the "router" object that will be accessed in our "index.js" file

router.get("/all", (req, res) => {
    courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
})

router.get("/", (req, res) => {
    courseController.getAllActive().then(resultFromController => res.send(resultFromController));
})


router.get("/", (req, res) => {
    courseController.getAllActive().then(resultFromController => res.send(resultFromController));
})

router.get("/:courseId", (req, res) => {
    console.log(req.params.courseId);
    courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
})

router.put("/:courseId", auth.verify, (req, res) => {
    let isAdmin = auth.decode(req.headers.authorization).isAdmin
    courseController.updateCourse(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));
})

router.put("/:courseId/archive", auth.verify, (req, res) => {
    let isAdmin = auth.decode(req.headers.authorization).isAdmin
    courseController.archiveCourse(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));
})

module.exports = router;