const express = require('express');
const router = express.Router();
const userController = require("../controllers/user.js");
const auth = require("../auth.js")

router.post("/checkEmail", (req, res) => {
    userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
});

router.post("/register", (req, res) =>{
    userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/login", (req,res) => {
    userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

router.post("/details",auth.verify,  (req, res) => {
    const userData = auth.decode(req.headers.authorization)
    userController.getProfile(userData).then(resultFromController => res.send(resultFromController))
});

router.post("/enroll", (req, res) => {
    const userData = auth.decode(req.headers.authorization)
    let data = {
        userId : auth.decode(req.headers.authorization).id,
        isAdmin : auth.decode(req.headers.authorization).isAdmin,
        courseId : req.body.courseId
    }
    userController.enroll(data).then(resultFromController => res.send(resultFromController))
})


// Allows us to export the "router" object that will be access in our index.js file
module.exports = router;